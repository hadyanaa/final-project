<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Final Project

## Kelompok 8

## Anggota Kelompok
* Hadyan Abdul Aziz

## Tema Project

Forum Tanya Jawab
Nama Forum ini adalah Islamoverflow. Terinspirasi dari web stackoverflow. Dsini semua orang dapat bertanya tentang agama Islam dan mendapatkan jawabannya.

## ERD

![gambar erd](database-io.png)

## Link Video
Link Demo Aplikasi  : https://youtu.be/p2qmNo6KlZw

Link Deploy         : http://www.ioverflow.sanbercodeapp.com

Notes: Deploy masih terkendala saat melakukan ekspor database. Akan diperbaiki segera. 
