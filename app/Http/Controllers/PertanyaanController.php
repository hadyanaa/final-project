<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = DB::table('pertanyaan')->get();
        return view ('items.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'title' => 'required|unique:pertanyaan',
            'isi_pertanyaan' => 'required',
            'gambar' => 'image|file|max:2048'
        ]);

        if($request->file('image')){
            $validatedData['image'] = $request->file('image')->store('post-images');
        }

        $validatedData['user_id'] = auth()->user()->id;

        // $query = DB::table('pertanyaan')-> insert([
        //     "title" => $request["title"],
        //     "isi_pertanyaan" => $request["isi_pertanyaan"],
        //     "gambar" => $request["gambar"],
        //     "user_id" => [Auth::id()]
        // ]);

        $pertanyaan = new Pertanyaan;
        $pertanyaan->title = $request["title"];
        $pertanyaan->isi_pertanyaan = $request["isi_pertanyaan"];
        // $pertanyaan->user_id = Auth::id();
        $pertanyaan->save();

        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = DB::table('pertanyaan')->where('id', $id)->first();

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'title' => 'required|unique:pertanyaan',
            'isi_pertanyaan' => 'required'
        ]);

        $query = DB::table('pertanyaan')
                ->where('id', $id)
                ->update([
                    'title' => $request['title'],
                    'isi_pertanyaan' => $request['isi_pertanyaan'],
                    'gambar' => $request['gambar']
                ]);
                return redirect('/pertanyaan')->with('success', 'Berhasil Update Pertanyaan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Post berhasil dihapus!');
    }
}
