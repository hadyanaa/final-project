<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('template.master');
// });

Route::get('/home', function(){
    return view('template.master');
});

/* Belajar
Route::get('/pertanyaan/create', 'PostController@create');
Route::post('/pertanyaan', 'PostController@store');
Route::get('/pertanyaan', 'PostController@index');
Route::get('/pertanyaan/{id}/edit', 'PostController@edit');
Route::put('/pertanyaan/{id}', 'PostController@update');
Route::delete('/pertanyaan/{id}', 'PostController@destroy');
*/

Route::resource('pertanyaan', 'PertanyaanController');
Route::resource('jawaban', 'JawabanController');
Route::resource('kategori', 'KategoriController');
Route::resource('profile', 'ProfileController');


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/profile', 'ProfileController@store');
