@extends('template.master')

@section('content')
<div class="card card-primary mt-2 ml-4 mr-4">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$post->title}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/pertanyaan/{{$post->id}}" >
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{old('title', $post->title)}}" placeholder="Tulis judul">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi_pertanyaan">Pertanyaan</label>
                    <input type="text" class="form-control" id="isi_pertanyaan" name="isi_pertanyaan"  value="{{ old('isi_pertanyaan', $post->isi_pertanyaan)}}" placeholder="Tulis pertanyaan">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="gambar">Gambar</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="gambar" name="gambar">
                        <label class="custom-file-label" for="gambar">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
@endsection