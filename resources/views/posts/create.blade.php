@extends('template.master')

@section('content')
<div class="card card-primary mt-2 ml-4 mr-4">
              <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/pertanyaan" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{old('title', '')}}" placeholder="Tulis judul">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi_pertanyaan">Pertanyaan</label>
                    <input type="text" class="form-control" id="isi_pertanyaan" name="isi_pertanyaan"  value="{{ old('isi_pertanyaan', '')}}" placeholder="Tulis pertanyaan">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="mb-3">
                    <label for="image" class="form-label">Upload Gambar</label>
                    <input class="form-control" type="file" id="image" name="image">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
@endsection