@extends('template.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-1">
    </div>
    <div class="col">

    <div class="card card-widget widget-user ">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
                <h3 class="widget-user-username">{{ Auth::user()->name}}</h3>
                <h5 class="widget-user-desc">{{Auth::user()->email}}</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="#" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">0000</h5>
                      <span class="description-text">Jumlah Pertanyaan</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">0000</h5>
                      <span class="description-text">Jumlah Jawaban</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header">35</h5>
                      <span class="description-text">PRODUCTS</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
    </div>
    <div class="col-md-1">
    </div>
  </div>
</div>
@endsection