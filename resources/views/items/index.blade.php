@extends('template.master')

@section('content')
<div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse($posts as $key => $post)
                    <tr>
                      <td>
                      <div class="col-md mr-4">
                            <div class="card card-danger" style="transition: all 0.15s ease 0s; height: inherit; width: inherit;">
                            <div class="card-header">
                                <h3 class="card-title ">{{ $post->title}} </h3>

                                <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i>
                                </button>
                                </div>
                                <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table class="table table-borderless">
                                <thead>
                                  <tr>
                                    <td class="col-10">{{$post->isi_pertanyaan}}</td>
                                    <td class="col" style="display: flex;">
                                      <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-info btn-sm ">edit</a>
                                      <form action="/pertanyaan/{{$post->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm"></form></td>
                                  </tr>
                                  <tr>
                                    <td class="col-10">
                                      @if ($post->image)
                                      <img src="{{asset('storage/' . $post->image)}}" alt="">
                                      @else
                                      {{$post->image}}
                                      @endif
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label" >Tulis Jawaban</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="tulis jawaban disini..."></textarea>
                                      </div></td>
                                      <td>
                                      <a href="#" class="btn btn-info btn-sm bottom ">Kirim</a>
                                      </td>
                                  </tr>
                                </thead>
                              </table>
                                

                                
                            
                            </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                      </td>
                    </tr>
                    @empty
                    <p>No Questions</p>
                    @endforelse
                  </tbody>
                </table>
              </div>

@endsection